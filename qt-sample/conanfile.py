from conans import ConanFile, CMake
from conan.tools.cmake import CMakeDeps
from conans.errors import ConanException


# from conans.client.generators.cmake_paths import CMakePathsGenerator

class QtApp(ConanFile):
    settings = "os", "arch", "compiler", "build_type"

    def requirements(self):
        self.requires("qt/5.15.5")
        self.requires("openssl/1.1.1q", override=True)

    # def build_requirements(self):
    #     self.tool_requires("cmake/3.22.3")

    # def layout(self):
    #     cmake_layout(self)

    # def package_info(self):
    #     self.cpp_info.set_property("cmake_find_mode", "module")

    # populates the self.generators_folder
    # e.g. "build-win11-debug/conan/conan_toolchain.cmake"
    def generate(self):
        # generates e.g. "Qt5Config.cmake"
        deps = CMakeDeps(self)
        deps.generate()

    default_options = {"qt:shared": True, "openssl:shared": True}

    # def imports(self):
    #     self.copy("*.dll", dst="bin", src="bin")
    #     self.copy("*.dylib", dst="bin", src="lib")

    def configure_cmake(self):
        cmake = CMake(self)

        # Re-used between build() and package()
        cmake.definitions["SOME_DEFINITION_NAME"] = "On"
        cmake.verbose = True

        try:
            cmake.configure()
        except ConanException as err:
            print("Conan Error: {0}".format(err))
            raise
        except ValueError as err:
            print("Value Error: Could not convert data to an integer.".format(err))
        except BaseException as err:
            print(f"Unexpected {err=}, {type(err)=}")
            raise
        return cmake

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

    def build(self):
        cmake = self.configure_cmake()
        cmake.build()
