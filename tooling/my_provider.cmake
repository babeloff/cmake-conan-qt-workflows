# Always ensure we have the policy settings this provider expects
cmake_minimum_required(VERSION 3.24)

set(MY_CONAN_PROVIDER_INSTALL_DIR ${CMAKE_BINARY_DIR}/my_conan_packages
        CACHE PATH "The directory this provider installs packages to"
        )
# Tell the built-in implementation to look in our area first, unless
# the find_package() call uses NO_..._PATH options to exclude it
list(APPEND CMAKE_MODULE_PATH ${MY_CONAN_PROVIDER_INSTALL_DIR}/cmake)
list(APPEND CMAKE_PREFIX_PATH ${MY_CONAN_PROVIDER_INSTALL_DIR})

macro(my_conan_provide_dependency method package_name)
    execute_process(
            COMMAND some_tool ${package_name} --installdir ${MY_CONAN_PROVIDER_INSTALL_DIR}
            COMMAND_ERROR_IS_FATAL ANY
    )
endmacro()

cmake_language(
        SET_DEPENDENCY_PROVIDER my_conan_provide_dependency
        SUPPORTED_METHODS FIND_PACKAGE
)